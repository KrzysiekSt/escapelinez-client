#ifndef LINES_HPP
#define LINES_HPP
#include <SDL2/SDL.h>
#include "ConfigFile.hpp"


class Line
{
public:
    Line() = default;
    Line(int height);

    void update(float deltaTime);
    void draw() const;
    void updateSpeed(float newSpeed);
    void updateHeigth(float y);

private:
    struct Pos
    {
        float x = 0;
        float y = 0;
    };

    Pos m_mainPos;
    Pos m_fragPos;

    int m_width = 32;
    float m_speed = 32;
    
    int m_screenWidth = GlobalConfigFile.getWidth();

    bool m_showFrag = false;
    
    float m_timer = 0;
    bool m_once = true;
};

#endif
