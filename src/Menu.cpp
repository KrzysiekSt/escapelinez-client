#include "Menu.hpp"
#include "Button.hpp"
#include "Settings.hpp"
#include "ConfigFile.hpp"
#include "TextureLoader.hpp"
#include "Renderer.hpp"
#include "MenuOption.hpp"

void Menu::init(SDL_Window* win)
{
    Window = win;

    m_background = loadTexture("data/Images/Main/lines.png");

    changeState = StateType::None;

    GlobalHoverOptions = 5;

    singleButton.init("Menu/sin.png", "Menu/sinS.png", w / 2 - 150, h * 0.3, 1);
    multiButton.init("Menu/mul.png", "Menu/mulS.png", w / 2 - 150, h*0.4, 2);
    settingsButton.init("Menu/sett.png", "Menu/settS.png", w / 2 - 150, h*0.5, 3);
    exitButton.init("Menu/ex.png", "Menu/exS.png", w / 2 - 150, h*0.6, 4);
}

void Menu::processEvent(const SDL_Event& event)
{
    if(event.type == SDL_KEYDOWN)
    {
        switch(event.key.keysym.sym)
        {
            case SDLK_TAB:
                GlobalHover++;
            break;
            case SDLK_DOWN:
                GlobalHover++;
            break;
            case SDLK_UP:
                GlobalHover--;
            break;
            case SDLK_RETURN:
                switch(GlobalHover % 5)
                {
                    case 1:
                        changeState = StateType::OfflineMod;
                    break;
                    case 2:
                        changeState = StateType::WaitingRoom;
                    break;
                    case 3:
                        changeState = StateType::Settings;
                    break;
                    case 4:
                        changeState = StateType::Exit;
                    break;
                }
            break;
        }
    }
}

void Menu::update(float deltaTime)
{
    if (singleButton.isPressed())
    {
        changeState = StateType::OfflineMod;
    }
    else if (multiButton.isPressed())
    {
        changeState = StateType::WaitingRoom;
    }
    else if (settingsButton.isPressed())
    {
        changeState = StateType::Settings;
    }
    else if (exitButton.isPressed())
    {
        changeState = StateType::Exit;
    }

}

void Menu::draw()
{
    SDL_RenderCopy(GlobalRenderer, m_background, NULL, NULL);

    if(singleButton.draw())
    {
        GlobalHover = singleButton.getId();
    }
    if(multiButton.draw())
    {
        GlobalHover = multiButton.getId();
    }
    if(settingsButton.draw())
    {
        GlobalHover = settingsButton.getId();
    }
    if(exitButton.draw())
    {
        GlobalHover = exitButton.getId();
    }

}

void Menu::quit()
{
    GlobalHover = 0;
    SDL_DestroyTexture(m_background);
}

StateType Menu::nextState()
{
    return changeState;
}
