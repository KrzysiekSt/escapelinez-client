#include "Network.hpp"
#include <iostream>


const int		*ClientId;
int 			ServerId = 1;

std::vector<server> servers =
{
	server("Login Server", sf::IpAddress("serwerkrolak.ddns.net")),
	server("Loop Back", sf::IpAddress("127.0.0.1")),
};

MySocket		MySocketLogin;
MyUdpSocket     MyUdpSocketPlayers;
MySocket		MySocketToBattle;


void MySocket::send(std::string ToSend)
{
    socket.send(ToSend.c_str(), ToSend.length() + 1);
}

std::string MySocket::get()
{
    char message[128];
    std::size_t received;
    socket.receive(message, 128, received);
    std::string str = message;
    return str.substr(0, received);
}

int MySocket::getStatus()
{
    return status;
}

//---------------------------------------------------------------

void MyUdpSocket::send(std::string ToSend)
{
    socket.send(ToSend.c_str(), ToSend.length() + 1, MyUdpSocketIp, MyUdpSocketPort);
}

std::string MyUdpSocket::get()
{
    char message[128];
    std::size_t received;
    socket.receive(message, 128, received, MyUdpSocketIp, MyUdpSocketPort);
    std::string str = message;
    return str.substr(0, received);
}

int MyUdpSocket::getStatus()
{
    return status;
}
