#ifndef BUTTON_HPP
#define BUTTON_HPP
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>

class Button
{
public:
    Button() = default;
    ~Button();

    // void init(const std::string& released, const std::string& selected, int x, int y, int w = 300);
    void init(const std::string& released, const std::string& selected, int x, int y, int id = 0, int w = 300);

    bool draw();

    int getId()
    {
        return m_id;
    }

    bool isPressed() const;
    bool isHover() const;

    // void setHoverTrue()
    // {
    //     m_hover = true;
    // }
    //
    // void setHoverFalse()
    // {
    //     m_hover = false;
    // }


private:
    SDL_Texture* m_released = nullptr;
    SDL_Texture* m_selected = nullptr;
    SDL_Rect     m_rect;

    int          m_initTime = 0;
    int          m_id;
    bool         m_hover = false;
};

#endif /* BUTTON_HPP */
