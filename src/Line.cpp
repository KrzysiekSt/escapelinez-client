#include "Line.hpp"
#include "Renderer.hpp"
#include "ConfigFile.hpp"
#include <iostream>

Line::Line(int height)
{
    int x = rand() % GlobalConfigFile.getWidth();

    m_mainPos = {x, height};
    m_fragPos = {x, height};

    m_width = (rand() % int(GlobalConfigFile.getWidth() * 0.1)) + (GlobalConfigFile.getWidth() * 0.8);

    m_speed = ((rand() % 400) - 200) * 1000;
}

void Line::update(float deltaTime)
{
    m_timer += deltaTime;
    
    if (m_timer > 0.5 && m_once)
    {
        m_speed /= 1000;

        m_once = false;
    }
    
    m_mainPos.x -= m_speed * deltaTime;
    
    if(m_speed > 0)
    {
        if (m_mainPos.x < 0)
        {
            m_showFrag = true;

            m_fragPos.x = (m_screenWidth - abs(m_mainPos.x));

            if (m_fragPos.x < (m_screenWidth - m_width))
                m_mainPos.x = (m_screenWidth - m_width);
        }
        else
            m_showFrag = false;
    }
    if(m_speed < 0)
    {
        if (m_mainPos.x > m_screenWidth)
        {
            m_showFrag = true;

            m_fragPos.x = (abs(m_mainPos.x) - m_screenWidth);

            if (m_fragPos.x > (m_width))
                m_mainPos.x = (m_width);
        }
        else
            m_showFrag = false;
    
    }

    
}

void Line::draw() const
{
    if(m_speed > 0)
    {
        SDL_SetRenderDrawColor(GlobalRenderer, 0, 64, 240, 255);
        SDL_RenderDrawLine(GlobalRenderer, m_mainPos.x, m_mainPos.y, m_mainPos.x + m_width, m_mainPos.y);

        if (m_showFrag)
            SDL_RenderDrawLine(GlobalRenderer, m_fragPos.x, m_fragPos.y, m_fragPos.x + m_width, m_fragPos.y);
        
    }
    if(m_speed < 0)
    {
        SDL_SetRenderDrawColor(GlobalRenderer, 0, 64, 240, 255);
        SDL_RenderDrawLine(GlobalRenderer, m_mainPos.x, m_mainPos.y, m_mainPos.x - m_width, m_mainPos.y);

        if (m_showFrag)
            SDL_RenderDrawLine(GlobalRenderer, m_fragPos.x, m_fragPos.y, m_fragPos.x - m_width, m_fragPos.y);
        
    }
        
}


void Line::updateSpeed(float newSpeed)
{
    m_speed = newSpeed;
    //std::cout << "new spped:"<< newSpeed << '\n';
}

void Line::updateHeigth(float y)
{
    m_mainPos.y -= y;
    m_fragPos.y -= y;
}
