#include "Menu.hpp"
#include "Button.hpp"
#include "Settings.hpp"
#include "ConfigFile.hpp"
#include "TextureLoader.hpp"
#include "Renderer.hpp"
#include "OfflineMod.hpp"
#include "MenuOption.hpp"


void OfflineMod::init(SDL_Window* win)
{
    Window = win;

    m_background = loadTexture("data/Images/Main/lines.png");

    changeState = StateType::None;

    GlobalHoverOptions = 3;

    CatchMeIfYouCanButton.init("OfflineMod/CatchMe.png", "OfflineMod/CatchMeS.png", w / 2 - 150, h * 0.3, 1);
    FindTheWayInsideButton.init("OfflineMod/FindThe.png", "OfflineMod/FindTheS.png", w / 2 - 150, h*0.4, 2);
}

void OfflineMod::processEvent(const SDL_Event& event)
{
    if(event.type == SDL_KEYDOWN)
    {
        switch(event.key.keysym.sym)
        {
            case SDLK_TAB:
                GlobalHover++;
            break;
            case SDLK_DOWN:
                GlobalHover++;
            break;
            case SDLK_UP:
                GlobalHover--;
            break;
            case SDLK_RETURN:
                switch(GlobalHover % 3)
                {
                    case 1:
                        changeState = StateType::CatchMe;
                    break;
                    case 2:
                        changeState = StateType::FindTheWayInside;
                    break;
                }
            break;
        }
    }
}

void OfflineMod::update(float deltaTime)
{
    if (CatchMeIfYouCanButton.isPressed())
    {
        changeState = StateType::CatchMe;
    }
    else if (FindTheWayInsideButton.isPressed())
    {
        changeState = StateType::FindTheWayInside;
    }

}

void OfflineMod::draw()
{
    SDL_RenderCopy(GlobalRenderer, m_background, NULL, NULL);

    if(CatchMeIfYouCanButton.draw())
    {
        GlobalHover = CatchMeIfYouCanButton.getId();
    }
    if(FindTheWayInsideButton.draw())
    {
        GlobalHover = FindTheWayInsideButton.getId();
    }
}

void OfflineMod::quit()
{
    GlobalHover = 0;
    SDL_DestroyTexture(m_background);
}

StateType OfflineMod::nextState()
{
    return changeState;
}
