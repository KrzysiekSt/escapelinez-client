#ifndef NETWORK_HPP
#define NETWORK_HPP
#include <SFML/Network.hpp>
#include <string>
#include <iostream>

enum SocketStatus
{
    SOCKET_STATUS_OK,
    SOCKET_STATUS_NOT_READY,
    SOCKET_STATUS_PARTIAL,
    SOCKET_STATUS_DISCONNECTED,
    SOCKET_STATUS_UNEXPECTED_ERROR
};

class MySocket
{
public:
    MySocket() = default;
    MySocket(sf::IpAddress ip, int port)
        : MySocketIp(ip), MySocketPort(port)
    {
        status = socket.connect(ip, port);
        std::cout<<"Connected to :"<< socket.getRemoteAddress()<< ':'
                << socket.getRemotePort() << std::endl;

    }

    void send(std::string ToSend);
    std::string get();
    int getStatus();

private:
    sf::IpAddress MySocketIp;
    sf::TcpSocket socket;
    sf::Socket::Status status;
    int MySocketPort;
};

//--------------------------------------------------------

class MyUdpSocket
{
public:
    MyUdpSocket() = default;
    MyUdpSocket(sf::IpAddress ip, unsigned short port)
        : MyUdpSocketIp(ip), MyUdpSocketPort(port)
    {
        std::cout<<"Udp Socket Created: "<< ip <<":"<<port<<std::endl;
        socket.bind(port);
    }

    void send(std::string ToSend);
    std::string get();
    int getStatus();

private:
    sf::IpAddress MyUdpSocketIp;
    sf::UdpSocket socket;
    sf::Socket::Status status;
    unsigned short MyUdpSocketPort;
};


//------------------------------------------------------------

extern MySocket             MySocketLogin;
extern MyUdpSocket          MyUdpSocketPlayers;
extern MySocket             MySocketToBattle;
extern const int            *ClientId;
extern int                  ServerId;

struct server
{
    std::string name;
    sf::IpAddress ip;

    server(std::string name, sf::IpAddress ip) :
        name(name), ip(ip)
    {}
};

extern std::vector<server> servers;

#endif /* NETWORK_HPP */
