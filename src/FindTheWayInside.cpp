#include "FindTheWayInside.hpp"
#include "ConfigFile.hpp"
#include "Renderer.hpp"
#include <math.h>

void FindTheWayInside::init(SDL_Window* win)
{
    Window = win;

    m_player.r = 25;

    m_maze = new Maze(1, m_player.r);
    m_maze->generate();

    m_player.x = (m_maze->getSquareSize() / 2) + m_maze->getOffsetW();
    m_player.y = (m_maze->getSquareSize() / 2) + m_maze->getOffsetH();

    m_CurrentSquare = m_maze->getMaze()[0];


    m_timer = 0;
}

void FindTheWayInside::update(float deltaTime)
{
    m_timer += deltaTime;
    SetCurrentSquare();
    CheckWall(deltaTime);
    // if (m_timer >= 20 && a)
    // {
    //     std::cout << m_maze->getM_backSize() << '\n';
    //     //m_maze->change(200);
    //     a = false;
    // }
}

void FindTheWayInside::draw()
{
    SDL_SetRenderDrawColor(GlobalRenderer, 255, 64, 0, 0);
    m_maze->draw(false);
    SDL_SetRenderDrawColor(GlobalRenderer, 255, 10, 98, 0);

     for(float x = (-40); x < 40; x+=1)
     {
         for(float y = (-40); y < 40; y+=1)
         {
             if(pow(x, 2) + pow(y, 2) < pow(m_player.r, 2))
             {
                 int a = x + m_player.x;
                 int b = y + m_player.y;
                 SDL_RenderDrawPoint(GlobalRenderer, a, b);
             }

         }
     }
}

void FindTheWayInside::SetCurrentSquare()
{
    if(m_CurrentSquare != m_maze->findSquare((m_player.x - m_maze->getOffsetW()) / m_maze->getSquareSize(),  (m_player.y - m_maze->getOffsetH()) / m_maze->getSquareSize()))
    {
        m_CurrentSquare = m_maze->findSquare((m_player.x - m_maze->getOffsetW()) / m_maze->getSquareSize(), (m_player.y - m_maze->getOffsetH()) / m_maze->getSquareSize());
        if(m_CurrentSquare->x1 == 0)
        {
            m_CurrentSquare->x1 = m_maze->getSquareSize() * m_CurrentSquare->x + m_maze->getOffsetW();
            m_CurrentSquare->x2 = m_maze->getSquareSize() * (m_CurrentSquare->x + 1) + m_maze->getOffsetW();
            m_CurrentSquare->y1 = m_maze->getSquareSize() * m_CurrentSquare->y + m_maze->getOffsetH();
            m_CurrentSquare->y2 = m_maze->getSquareSize() * (m_CurrentSquare->y + 1) + m_maze->getOffsetH();
        }
    }

}

void FindTheWayInside::CheckWall(float deltaTime)
{
    const Uint8* keys = SDL_GetKeyboardState(NULL);

    if (m_CurrentSquare != nullptr)
    {
        int leftWallDistance = m_CurrentSquare->x * m_maze->getSquareSize() + m_maze->getOffsetW() + m_player.r;
        int rightWallDistance = (m_CurrentSquare->x + 1) * m_maze->getSquareSize() + m_maze->getOffsetW() - m_player.r;
        int topWallDistance = m_CurrentSquare->y * m_maze->getSquareSize()  + m_maze->getOffsetH() + m_player.r;
        int downWallDistance = (m_CurrentSquare->y + 1) * m_maze->getSquareSize()  + m_maze->getOffsetH() - m_player.r;
        
        float PlayerBackX = m_player.x;
        float PlayerBackY = m_player.y;


        if (m_CurrentSquare->leftWall == false)
        {
            if (keys[SDL_SCANCODE_LEFT])
            {
                m_player.x -= deltaTime * 100;
            }
        }
        else if (leftWallDistance < m_player.x)
        {
            if (keys[SDL_SCANCODE_LEFT])
            {
                m_player.x -= deltaTime * 100;
            }
        }

        if (m_CurrentSquare->rightWall == false)
        {
            if (keys[SDL_SCANCODE_RIGHT])
            {
               m_player.x += deltaTime * 100;
            }
        }
        else if (rightWallDistance > m_player.x)
        {
            if (keys[SDL_SCANCODE_RIGHT])
            {
               m_player.x += deltaTime * 100;
            }
        }

        if (m_CurrentSquare->topWall == false)
        {
            if (keys[SDL_SCANCODE_UP])
            {
               m_player.y -= deltaTime * 100;
            }
        }
        else if (topWallDistance < m_player.y)
        {
            if (keys[SDL_SCANCODE_UP])
            {
               m_player.y -= deltaTime * 100;
            }
        }

        if (m_CurrentSquare->downWall == false)
        {
            if (keys[SDL_SCANCODE_DOWN])
            {
                m_player.y += deltaTime * 100;
            }
        }
        else if (downWallDistance > m_player.y)
        {
            if (keys[SDL_SCANCODE_DOWN])
            {
                m_player.y += deltaTime * 100;
            }
        }
        
        if(sqrt(pow(m_CurrentSquare->x1 - m_player.x, 2) + pow(m_CurrentSquare->y1 - m_player.y, 2)) - m_player.r < -0.5)
        {
            m_player.x = PlayerBackX;
            m_player.y = PlayerBackY;

        }
        if(sqrt(pow(m_CurrentSquare->x2 - m_player.x, 2) + pow(m_CurrentSquare->y1 - m_player.y, 2)) - m_player.r < -0.5)
        {
            m_player.x = PlayerBackX;
            m_player.y = PlayerBackY;
        }
        if(sqrt(pow(m_CurrentSquare->x1 - m_player.x, 2) + pow(m_CurrentSquare->y2 - m_player.y, 2)) - m_player.r < -0.5)
        {
            m_player.x = PlayerBackX;
            m_player.y = PlayerBackY;
        }
        if(sqrt(pow(m_CurrentSquare->x2 - m_player.x, 2) + pow(m_CurrentSquare->y2 - m_player.y, 2)) - m_player.r < -0.5)
        {
            m_player.x = PlayerBackX;
            m_player.y = PlayerBackY;
        }
    }
}

void FindTheWayInside::quit()
{

}
